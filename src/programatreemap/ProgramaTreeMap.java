package programatreemap;

import java.util.*;

/**
 * Clase ProgramaTreeMap que contiene el metodo main, y además los metodos para insertar productos y visualizarlos
 * @author Luis
 */
public class ProgramaTreeMap {

    /**
     * metodo para insertar 5 productos en un TreeMap
     * @param stock TreeMap con el stock de productos
     */
    public void insertarProductos(Map<String,Producto> stock){
        stock.put("1", new Producto("Producto 1", 10));
        stock.put("2", new Producto("Producto 2", 12));
        stock.put("3", new Producto("Producto 3", 8));
        stock.put("4", new Producto("Producto 4", 15));
        stock.put("5", new Producto("Producto 5", 25));
    }
    /**
     * metodo para visualizar todos los productos
     * @param stock TreeMap con el stock de productos
     */
    public void visualizarTodo(Map<String,Producto> stock){
        Producto p;
        String numSerie;
        System.out.println("Nº   Nombre       Precio");
        for(Map.Entry ent:stock.entrySet()){
            p = (Producto) ent.getValue();
            numSerie = (String) ent.getKey();
            System.out.println(numSerie+"   "+p.getNombre()+"    "+p.getPrecio());
        }
    }
    /**
     * metodo main donde creamos el TreeMap y llamamos a los metodos introducir productos y visualizarlos
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Map<String,Producto> stock = new TreeMap<>();
        ProgramaTreeMap obj = new ProgramaTreeMap();
        obj.insertarProductos(stock);
        obj.visualizarTodo(stock);
    }
}
