package programatreemap;

/**
 * Clase Producto, que va a tener los atributos nombre, precio y metodos gets y sets
 * @author Luis
 */
public class Producto {
 
    /**
     * El nombre del producto
     */
    private String nombre;
    /**
     * El precio del producto
     */
    private double precio;
    /**
     * constructor por defecto sin parámetros
     */
    public Producto() {
    }
    /**
     * Constructor con parámetros para crear objectos productos con el nombre y precio enviados como parametros
     * @param nombre Nombre del producto
     * @param precio Precio del producto
     */
    public Producto(String nombre, double precio) {
        this.nombre = nombre;
        this.precio = precio;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the precio
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
